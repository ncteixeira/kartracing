﻿using FluentAssertions;
using KartRacing;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Xunit;

namespace KartRacingTest
{
    public class RaceDataTest
    {
        [Fact]
        public void ReturnRaceLogLenghtGivenAValidLogPath()
        {

            var binDirectory = "bin";
            var path = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf(binDirectory));
            var fileName = "RaceResultTest.txt";

            var data = new RaceData().GetRaceResult(path, fileName);
            data.Should().HaveCount(23);
        }
    }
}
