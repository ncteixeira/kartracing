﻿using FluentAssertions;
using KartRacing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace KartRacingTest
{
    public class RaceResultTest
    {
        [Fact]
        public void ReturnRaceLogLegPath()
        {
            var laps = new List<Lap>();
            laps.Add(new Lap
            {
                DriverCode = 2,
                DriverName = "R.BARRICHELLO",
                Laps = 1,
                LapTime = TimeSpan.Parse("0:1:02.002")
            });

            laps.Add(new Lap
            {
                DriverCode = 1,
                DriverName = "F.Massa",
                Laps = 1,
                LapTime = TimeSpan.Parse("0:1:02.002")
            });

            laps.Add(new Lap
            {
                DriverCode = 1,
                DriverName = "F.Massa",
                Laps = 2,
                LapTime = TimeSpan.Parse("0:2:02.004")
            });

            var data = new RaceResult().GetDriverResult(laps);
            data.Should().HaveCount(2);
            data.FirstOrDefault().TotalLaps.Should().Equals(2);
            data.FirstOrDefault().Time.Should().BeGreaterThan(TimeSpan.Parse("00:03:04.000"));

        }
    }
}
