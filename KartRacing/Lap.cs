﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KartRacing
{
    public class Lap
    {
        public int DriverCode { get; set; }
        public string DriverName { get; set; }
        public int Position { get; set; }
        public DateTime Time { get; set; }
        public TimeSpan  LapTime { get; set; }
        public int Laps { get; set; }
    }
}
