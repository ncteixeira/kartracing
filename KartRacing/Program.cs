﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace KartRacing
{
    class Program
    {
        private const string BIN_DIRECTORY = "bin";
        private const string FILE_NAME = "RaceResult.txt";
        static void Main(string[] args)
        {
            Console.WriteLine("Aguarde...");
            var serviceProvider = new ServiceCollection()
              .AddScoped<IRaceData, RaceData>()
              .AddScoped<IRaceResult, RaceResult>()
              .BuildServiceProvider();

            var raceData = serviceProvider.GetService<IRaceData>();
            var raceResult = serviceProvider.GetService<IRaceResult>();

            var path = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf(BIN_DIRECTORY));
            var laps = raceData.GetRaceResult(path, FILE_NAME);

            WriteResult(laps, raceResult);
            Console.Read();
        }

        static void WriteResult(IList<Lap> laps, IRaceResult raceResult)
        {
            if (laps.Count > 0)
            {
                var driverResult = new List<Race>();
                driverResult = raceResult.GetDriverResult(laps).ToList();

                if (driverResult.Count > 0)
                {
                    driverResult.OrderByDescending(p => p.TotalLaps).OrderBy(t => t.Time).ToList();

                    Console.Clear();
                    Console.WriteLine("Pos, Cód, Nome, Voltas, Tempo");

                    var postion = 0;
                    foreach (var item in driverResult)
                    {
                        Console.WriteLine($"{ ++postion },   { item.DriverCode}, { item.DriverName}, {item.TotalLaps },  { item.Time} ");
                    }
                }
            }
        }
    }
}
