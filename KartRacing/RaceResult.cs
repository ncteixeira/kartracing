﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KartRacing
{
    public class RaceResult :IRaceResult
    {
        public IList<Race> GetDriverResult(IList<Lap> lapResult)
        {
            var driverResult = new List<Race>();
            if (lapResult.Count > 0)
            {
                var driverCodes = lapResult.GroupBy(d => d.DriverCode).Select(s => s.First().DriverCode).ToList();

                driverResult = GetStatics(driverCodes, lapResult).ToList();
                if (driverResult.Count > 0)
                {
                    driverResult = driverResult.OrderBy(t => t.Time).OrderByDescending(p => p.TotalLaps).ToList();
                }
            }
            return driverResult;
        }

        private IList<Race> GetStatics(IList<int> driveCodes, IList<Lap> laps)
        {
            var driverResult = new List<Race>();

            if (driveCodes.Count > 0)
            {
                foreach (var code in driveCodes)
                {
                    var driverData = laps.Where(d => d.DriverCode == code);
                    var driveName = driverData.FirstOrDefault().DriverName;
                    var raceResult = new Race
                    {
                        DriverCode = code,
                        DriverName = driveName
                    };

                    foreach (var item in driverData)
                    {
                        if (driverData.FirstOrDefault() == item)
                        {
                            raceResult.DriverName = item.DriverName;
                        }

                        raceResult.Time += item.LapTime;
                        raceResult.TotalLaps = item.Laps;

                    }
                    driverResult.Add(raceResult);
                }
            }
            return driverResult;
        }
    }
}
