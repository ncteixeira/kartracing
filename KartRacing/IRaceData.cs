﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KartRacing
{
    public interface IRaceData
    {
       List<Lap> GetRaceResult(string path, string fileName);
    }
}
