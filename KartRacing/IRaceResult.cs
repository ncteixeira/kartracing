﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KartRacing
{
    public interface IRaceResult
    {
        IList<Race> GetDriverResult(IList<Lap> lapResult);
    }
}
