﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace KartRacing
{
    public class RaceData : IRaceData
    {
        private const int DRIVE_CODE_POSTION = 1;
        private const int DRIVE_NAME_POSTION = 3;
        private const int LAP_POSTION = 4;
        private const int LAP_TIME_POSTION = 5;
        

        public List<Lap> GetRaceResult(string path, string fileName)
        { 
            var file = $"{path}\\{fileName}";
            var laps = new List<Lap>();

            if (Directory.Exists(path) && File.Exists(file))
            {
                var fileStream = new FileStream(file, FileMode.Open);

                using (StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    var line = string.Empty;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        var lines = line.Split(" ")
                                        .Where(x => !string.IsNullOrEmpty(x))
                                        .ToArray();

                        laps.Add(new Lap
                        {
                            DriverCode = Convert.ToInt32(lines[DRIVE_CODE_POSTION].Replace("\t", "")),
                            DriverName = lines[3].Replace("\t", ""),
                            Laps = Convert.ToInt32(lines[LAP_POSTION].Replace("\t", "")),
                            LapTime = TimeSpan.Parse($"{0}:{lines[LAP_TIME_POSTION]}")
                        });
                    }
                }
            }
            return laps;
        }
    }
}
