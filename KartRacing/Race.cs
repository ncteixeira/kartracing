﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KartRacing
{
    public class Race
    {
        public int DriverCode { get; set; }
        public string DriverName { get; set; }
        public int Position { get; set; }
        public TimeSpan Time { get; set; }
        public int TotalLaps { get; set; }
    }
}
